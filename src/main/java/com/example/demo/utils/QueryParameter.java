package com.example.demo.utils;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.HashMap;
import java.util.Map;


/**
 * 通用查询参数
 *
 * @author YZD
 */
@Getter
@Setter
@ToString
public class QueryParameter implements java.io.Serializable {

    /**
     * 当前第几页
     */
    private int pageNum = 1;

    /**
     * 每页多少条数据
     */
    private int pageSize = 10;

    /**
     * 搜索参数
     */
    private Map<String, Object> params = new HashMap<>();

}
