package com.example.demo.schedule;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;


/**
 * 定时任务
 *
 * @author YZD
 */
@Slf4j
@Component
public class ScheduleSync {


    /**
     * 每20秒执行一次 表达式：0/20 * * * * ?
     * 每10分钟执行一次 表达式：0 0/10 * * * ?
     * 每日凌晨执行一次 表达式：0 0 0 * * ?
     */
    @Scheduled(cron = "0/30 * * * * ?")
    public void test() {
        log.info("定时任务已执行" + LocalDateTime.now());
    }

}
